import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoqPage } from './roq.page';

const routes: Routes = [
  {
    path: '',
    component: RoqPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoqPageRoutingModule {}
