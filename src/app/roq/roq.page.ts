import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmailService } from '../services/email.service';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-roq',
  templateUrl: './roq.page.html',
  styleUrls: ['./roq.page.scss'],
})
export class RoqPage implements OnInit {
  email: FormGroup;

  destroy$: Subject<boolean> = new Subject<boolean>();
  constructor(private fb: FormBuilder, private emailService: EmailService) { }

  ngOnInit() {
    this.email = this.fb.group({
      name: [''],
      email: [''],
      body: ['']
    });
  }

  onSubmit() {
    // this.emailService.sendEmail(this.email.value).pipe(take(1)).subscribe(data => {
    //   console.log('Server Response:', data);
    //   this.email.reset();
    // });
    this.emailService.requestQuote(this.email.value).pipe(take(1)).subscribe(data => {
      console.log('Server Response:', data);
      this.email.reset();
    });
  }

  // getEmails() {
  //   this.emailService.getEmails().pipe(takeUntil(this.destroy$)).subscribe((emails: any[]) => {
  //     console.log(emails);
  //   });
  // }

}
