import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoqPage } from './roq.page';

describe('RoqPage', () => {
  let component: RoqPage;
  let fixture: ComponentFixture<RoqPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoqPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoqPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
