import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoqPageRoutingModule } from './roq-routing.module';

import { RoqPage } from './roq.page';
import { J2Module } from '../components/j2.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RoqPageRoutingModule,
    J2Module
  ],
  declarations: [RoqPage]
})
export class RoqPageModule {}
