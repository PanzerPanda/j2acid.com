import { Component, OnInit } from '@angular/core';
import { ScreensizeService } from 'src/app/services/screensize.service';
import { Router } from '@angular/router';

@Component({
  selector: 'j2-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  isDesktop: boolean;
  constructor(private ssService: ScreensizeService, private router: Router) {
    this.ssService.isDesktopView().subscribe(isDesktop => {
      console.log('is desktop changed: ', isDesktop);
      this.isDesktop = isDesktop;
    })
  }

  ngOnInit() { }

  navigateTo(value) {
    if (value) {
        this.router.navigate([value]);
    }
    return false;
}

}
