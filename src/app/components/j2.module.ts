import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { OilpricegraphComponent } from './oilpricegraph/oilpricegraph.component';
import { ImagecarouselComponent } from './imagecarousel/imagecarousel.component';



@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    OilpricegraphComponent,
    ImagecarouselComponent
  ],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    OilpricegraphComponent,
    ImagecarouselComponent
  ]
})
export class J2Module { }
