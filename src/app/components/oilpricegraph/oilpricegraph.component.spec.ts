import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OilpricegraphComponent } from './oilpricegraph.component';

describe('OilpricegraphComponent', () => {
  let component: OilpricegraphComponent;
  let fixture: ComponentFixture<OilpricegraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OilpricegraphComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OilpricegraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
