import { Component, OnInit, Renderer2, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import postscribe from 'postscribe';

@Component({
  selector: 'oilpricegraph',
  templateUrl: './oilpricegraph.component.html',
  styleUrls: ['./oilpricegraph.component.scss'],
})
export class OilpricegraphComponent implements OnInit, AfterViewInit {

  constructor(private renderer2: Renderer2, private el: ElementRef) { }

  ngOnInit() { }

  ngAfterViewInit() {
    postscribe('#CrudePriceDisplay', '<script src="https://www.oil-price.net/TABLE2/gen.php?lang=en"></script>');
    postscribe('#BrentPriceDisplay', '<script src="https://www.oil-price.net/widgets/brent_crude_price_large/gen.php?lang=en"></script>');
    postscribe('#CommoditiesPriceDisplay', '<script src="https://www.oil-price.net/COMMODITIES/gen.php?lang=en"></script>');
    postscribe('#GasPrices', '<script src="https://www.gas-cost.net/widget.php?lang=en"></script>');
  }  
}
