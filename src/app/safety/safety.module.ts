import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SafetyPageRoutingModule } from './safety-routing.module';

import { SafetyPage } from './safety.page';
import { J2Module } from '../components/j2.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SafetyPageRoutingModule,
    J2Module
  ],
  declarations: [SafetyPage]
})
export class SafetyPageModule {}
