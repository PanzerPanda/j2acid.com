import { Component, OnInit } from '@angular/core';


declare var RSSParser;


@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {

  SIZE = 8;
  CORS_PROXY = 'https://cors-anywhere.herokuapp.com/'
  RZ_LASTEST = 'https://www.rigzone.com/news/rss/rigzone_latest.aspx'
  EIA_DAILY = 'https://www.eia.gov/rss/todayinenergy.xml'; // will not respond
  OILGAS_JOURNAL_GI = 'http://feeds.feedburner.com/OilGasJournal-GeneralInterest?format=xml';
  OILGAS_IQ_NEWS = 'https://www.oilandgasiq.com/rss/news';
  OILGAS360_FEED = 'https://www.oilandgas360.com/360_articles/feed/'

  targetUrl: string;
  rzLatest: Array<any>;
  ogjGI: Array<any>;
  ogIQ: Array<any>;
  og360: Array<any>;
  parser = new RSSParser();

  constructor() { }

  ngOnInit() {
    this.parser.parseURL(this.CORS_PROXY + this.RZ_LASTEST, (err, feed) => {
      console.log(feed);
      console.log(feed.items);
      this.rzLatest = feed.items.slice(0, this.SIZE);;
    });
    this.parser.parseURL(this.CORS_PROXY + this.OILGAS_JOURNAL_GI, (err, feed) => {
      console.log(feed);
      console.log(feed.items);
      this.ogjGI = feed.items.slice(0, this.SIZE);;
    });
    this.parser.parseURL(this.CORS_PROXY + this.OILGAS_IQ_NEWS, (err, feed) => {
      console.log(feed);
      console.log(feed.items);
      this.ogIQ = feed.items.slice(0, this.SIZE);;
    });
    this.parser.parseURL(this.CORS_PROXY + this.OILGAS360_FEED, (err, feed) => {
      console.log(feed);
      console.log(feed.items);
      this.og360 = feed.items.slice(0, this.SIZE);;
    });
  }
}
