import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  truckImages = [
    {
      image: 'assets/J2Trucks/Killtruck_01_alt06.jpg',
      thumbImage: 'assets/J2Trucks/Killtruck_01_alt06.jpg'
    },
    {
      image: 'assets/J2Trucks/Killtruck_03_alt02.jpg',
      thumbImage: 'assets/J2Trucks/Killtruck_03_alt02.jpg'
    },
    {
      image: 'assets/J2Trucks/Pump_TorqTest_alt01.jpg',
      thumbImage: 'assets/J2Trucks/Pump_TorqTest_alt01.jpg'
    },
    {
      image: 'assets/J2Trucks/Killtruck_01_alt02.jpg',
      thumbImage: 'assets/J2Trucks/Killtruck_01_alt02.jpg'
    },
    {
      image: 'assets/J2Trucks/Van_Data_01_alt01.jpg',
      thumbImage: 'assets/J2Trucks/Van_Data_01_alt01.jpg'
    },
    {
      image: 'assets/J2Trucks/Van_Treater_01_alt01.jpg',
      thumbImage: 'assets/J2Trucks/Van_Treater_01_alt01_edit02.jpg'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  route(event) {
    console.log(event);
  }

}
