import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { J2Module } from '../components/j2.module';
import { NgImageSliderModule } from 'ng-image-slider'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    J2Module,
    NgImageSliderModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
