import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CsPageRoutingModule } from './cs-routing.module';

import { CsPage } from './cs.page';
import { J2Module } from '../components/j2.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CsPageRoutingModule,
    J2Module
  ],
  declarations: [CsPage]
})
export class CsPageModule {}
