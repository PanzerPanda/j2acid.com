import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CsPage } from './cs.page';

describe('CsPage', () => {
  let component: CsPage;
  let fixture: ComponentFixture<CsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
