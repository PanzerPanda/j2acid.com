import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private http: HttpClient) { }

  rootURL = '/email';

  requestQuote(email: any) {
    return this.http.post(this.rootURL + '/requestquote', {email});
  }

  // sendEmail(email: any) {
  //   return this.http.post(this.rootURL + '/send', {email});
  // }

  // getEmails() {
  //   return this.http.get(this.rootURL + '/testResponse');
  // }
}
