import { Component, OnInit } from '@angular/core';
import { ScreensizeService } from '../services/screensize.service';

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.page.html',
  styleUrls: ['./equipment.page.scss'],
})

export class EquipmentPage implements OnInit {

  isDesktop: boolean;
  killTrucks = [
    {
      image: '/assets/J2Trucks/Killtruck_01_alt06.jpg',
      thumbImage: '/assets/J2Trucks/Killtruck_01_alt06.jpg',
      alt: 'Killtruck 01 photo 1'
    },
    {
      image: '/assets/J2Trucks/Killtruck_01_alt02.jpg',
      thumbImage: '/assets/J2Trucks/Killtruck_01_alt02.jpg',
      alt: 'Killtruck 01 photo 2'
    },
    {
      image: '/assets/J2Trucks/Killtruck_03_alt01.jpg',
      thumbImage: '/assets/J2Trucks/Killtruck_03_alt01.jpg',
      alt: 'Killtruck 03 photo 1'
    },
    {
      image: '/assets/J2Trucks/Killtruck_03_alt02.jpg',
      thumbImage: '/assets/J2Trucks/Killtruck_03_alt02.jpg',
      alt: 'Killtruck 03 photo 2'
    }
  ]
  acidTransports = [
    {
      image: '/assets/J2Trucks/Trailer_Transport_01_alt01.jpg',
      thumbImage: '/assets/J2Trucks/Trailer_Transport_01_alt01.jpg',
      alt: 'Transport Trailer 01 photo 1'
    },
    {
      image: '/assets/J2Trucks/Trailer_Transport_01_alt02.jpg',
      thumbImage: '/assets/J2Trucks/Trailer_Transport_01_alt02.jpg',
      alt: 'Transport Trailer 01 photo 2'
    },
    {
      image: '/assets/J2Trucks/Trailer_Transport_03_alt01.jpg',
      thumbImage: '/assets/J2Trucks/Trailer_Transport_03_alt01.jpg',
      alt: 'Transport Trailer 03 photo 1'
    },
    {
      image: '/assets/J2Trucks/Trailer_Transport_01_alt01.jpg',
      thumbImage: '/assets/J2Trucks/Trailer_Transport_01_alt01.jpg',
      alt: 'Transport Trailer 06 photo 1'
    },
  ]
  fracPumps = [
    {
      image: '/assets/J2Trucks/Pump_Frac_01_alt01.jpg',
      thumbImage: '/assets/J2Trucks/Pump_Frac_01_alt01.jpg',
      alt: 'Frac Pump photo 1'
    }
  ]
  treaterVans = [
    {
      image: '/assets/J2Trucks/Van_Treater_01_alt01_edit.jpg',
      thumbImage: '/assets/J2Trucks/Van_Treater_01_alt01_edit.jpg',
      alt: 'Treater Van 01 photo 1'
    }
  ]
  showerTrailers = [
    {
      image: '/assets/J2Trucks/Trailer_Shower_01_alt01.jpg',
      thumbImage: '/assets/J2Trucks/Trailer_Shower_01_alt01.jpg',
      alt: 'Shower Trailer 01 photo 1'
    }
  ]
  ttPumps = [
    {
      image: '/assets/J2Trucks/Pump_Oil_01_alt01.jpg',
      thumbImage: '/assets/J2Trucks/Pump_Oil_01_alt01.jpg',
      alt: 'Oil Pump photo 1'
    }
  ]
  chemicals = [
    {
      image: '/assets/J2Trucks/_chemical2.jpg',
      thumbImage: '/assets/J2Trucks/_chemical2.jpg',
      alt: 'Acid Blends photo 1'
    }
  ]
  equipment: EquipmentDescription[] = [
    {
      imageSrc: '/assets/J2Trucks/Killtruck_01_alt06.jpg',
      title: 'Acid Killtrucks',
      subtitle: '5k and 10k'
    },
    {
      imageSrc: '/assets/J2Trucks/Trailer_Transport_04_alt01.jpeg',
      title: 'Acid Transports',
      subtitle: '5-36% capabilities'
    },
    {
      imageSrc: '/assets/J2Trucks/Pump_Frac_01_alt01.jpg',
      title: 'Single, Double, and 2500HHP Frac Pumps',
      subtitle: '1500-2500HHP all with data acquisition'
    },
    {
      imageSrc: '/assets/J2Trucks/Van_Treater_01_alt01.jpg',
      title: 'Treater Vans',
      description: 'Equipped with Lime software, ability to control one pump or upto an entire frac spread. Full Data acquisition equipped.'
    },
    {
      imageSrc: '/assets/J2Trucks/Trailer_Shower_01_alt01.jpg',
      title: 'Shower Trailers',
      description: '330 gal capacity, built to SPA specs'
    },
    {
      imageSrc: '/assets/J2Trucks/Pump_TorqTest_alt01.jpg',
      title: 'Torq & Test Pumps',
      description: '15000PSI pumps, equipped with hydrolic torq wrenches, rates available from .10 - 1.5 BBL/min. Data acquisition available.'
    },
    {
      imageSrc: '/assets/J2Trucks/_chemical2.jpg',
      title: 'Acid Blends and Chemicals',
      description: 'Full testing available with proprietary blends for every need.'
    }
  ]

  constructor(private ssService: ScreensizeService) {
    this.ssService.isDesktopView().subscribe(isDesktop => {
      console.log('is desktop changed: ', isDesktop);
      this.isDesktop = isDesktop;
    })
  }

  ngOnInit() {
  }

}

interface EquipmentDescription {
  imageSrc?: string;
  title: string;
  subtitle?: string;
  description?: string;
}
